# *******************************************************************************
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "${BASEDIR}/.cache")

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

git_tag=$(git tag --points-at HEAD)

if [ ! -d "${BASEDIR}/artifacts" ]; then
    mkdir -p "${BASEDIR}/artifacts"
else
    rm -f ${BASEDIR}/artifacts/gtgen-simulator_*.deb
fi

# Only create release artifact if current commit is tagged
if [ -n "$git_tag" ]; then
    echo "Creating release artifact for version: $git_tag"
    bazel build --config=gt_gen_release //Packaging:debian-gtgen-simulator
    bazel build --config=gt_gen_release //Packaging:debian-gtgen-cli

    echo "Copying release artifacts to artifacts folder ..."
    cp bazel-bin/Packaging/gtgen-simulator_*.deb "${BASEDIR}/artifacts"
    cp bazel-bin/Packaging/gtgen-cli_*.deb "${BASEDIR}/artifacts"
    echo "Generate test report complete! Release artifact is available at:"
    ls ${BASEDIR}/artifacts/gtgen-*.deb

else
    echo "Skipping generating code coverage for branch $BRANCH_NAME. Only tagged commit will generate release package."
fi
