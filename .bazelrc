########################################################################################################################
## Common bazel build configurations
########################################################################################################################
build:gt_gen --cxxopt=-std=c++17
build:gt_gen --compilation_mode=dbg
build:gt_gen --strip=never                     # Never strip debug information
build:gt_gen --features=ignore_deprecated_warnings
build:gt_gen --linkopt=-ldl
build:gt_gen --features=treat_warnings_as_errors
# disables warnings (adds -w and -Wno-error to compiler flags) for external dependencies
build:gt_gen --per_file_copt=+^external/.*,+.*\.pb\.cc,+^src/google/protobuf/.*@-w,-Wno-error
# disables warnings in gt-gen-simulator when including external dependency headers by using -isystem instead of -iquote
build:gt_gen --features=external_include_paths
build:gt_gen --config=default_warnings
build:gt_gen --config=strict_warnings

build:default_warnings --cxxopt="-Wall"
build:default_warnings --cxxopt="-Wold-style-cast"
build:default_warnings --cxxopt="-Wformat"

build:strict_warnings --cxxopt="-Wextra"
build:strict_warnings --cxxopt="-Wpedantic"
build:strict_warnings --cxxopt="-Wconversion"
build:strict_warnings --cxxopt="-Wfloat-equal"
build:strict_warnings --cxxopt="-Wformat-security"
build:strict_warnings --cxxopt="-Wshadow"
build:strict_warnings --cxxopt="-Wsign-conversion"


########################################################################################################################
# Common bazel test options
########################################################################################################################
test:gt_gen --test_output=errors
test:gt_gen --build_tests_only
test:gt_gen --test_verbose_timeout_warnings
test:gt_gen --test_strategy=standalone
test:gt_gen --test_env="GTEST_COLOR=1"  # Enable GoogleTest color output
test:gt_gen --trim_test_configuration
test:gt_gen --keep_going
test:gt_gen --define test_build=true


########################################################################################################################
# Release build config
########################################################################################################################
build:gt_gen_release --config=gt_gen
build:gt_gen_release --compilation_mode=opt
build:gt_gen_release --strip=always


########################################################################################################################
# Sanitizer build configs
########################################################################################################################
build:gt_gen_san --config=gt_gen
build:gt_gen_san --features=sanitizer
build:gt_gen_san --copt=-fsanitize=address
build:gt_gen_san --copt=-fno-omit-frame-pointer
build:gt_gen_san --linkopt=-fsanitize=address
