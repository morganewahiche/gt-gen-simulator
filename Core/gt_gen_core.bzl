load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v5.2.0"

def gt_gen_core():
    maybe(
        http_archive,
        name = "gt_gen_core",
        url = "https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/archive/{tag}/gt-gen-core-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "208ac5c364c8390d69198ce7fc56940695c65bd1c316cd22c3a102308bd8b064",
        strip_prefix = "gt-gen-core-{tag}".format(tag = _TAG),
        patches = ["@//Core:add_cstdint.patch"],
        patch_args = ["-p1"],
    )
