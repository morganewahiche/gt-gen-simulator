/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_TESTTRAFFICUPDATE_TESTTRAFFICUPDATE_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_TESTTRAFFICUPDATE_TESTTRAFFICUPDATE_H

#include "osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <osi_trafficupdate.pb.h>

#include <utility>

namespace gtgen::simulator::test_utils
{

class TestTrafficUpdate
{
  public:
    enum class TestHostVehicleMovement
    {
        kExternalDriver,
        kSimpleStub,
        kInternalMovement
    };

    explicit TestTrafficUpdate(TestHostVehicleMovement test_host_vehicle_movement);

    TestTrafficUpdate(const TestTrafficUpdate&) = delete;
    TestTrafficUpdate& operator=(const TestTrafficUpdate&) = delete;

    TestTrafficUpdate(TestTrafficUpdate&& rhs) = default;
    TestTrafficUpdate& operator=(TestTrafficUpdate&&) = default;

    ~TestTrafficUpdate() = default;

    void Step(const osi3::GroundTruth& gt);

    void SetTrafficUpdate(osi3::TrafficUpdate traffic_update) { traffic_update_ = std::move(traffic_update); }
    const osi3::TrafficUpdate& GetNewTrafficUpdate() { return traffic_update_; }
    void SetADFunctionActivationState(mantle_api::ExternalControlState state);

  private:
    void ModifyVehicleState(const osi3::GroundTruth& gt);

    osi3::TrafficUpdate traffic_update_{};

    mantle_api::ExternalControlState fake_activation_state_{mantle_api::ExternalControlState::kOff};
    TestHostVehicleMovement test_host_vehicle_movement_;
};

}  // namespace gtgen::simulator::test_utils

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_TESTTRAFFICUPDATE_TESTTRAFFICUPDATE_H
