<?xml version="1.0" encoding="UTF-8"?>
<OpenSCENARIO>
   <FileHeader revMajor="1" revMinor="1" date="2024-03-22T16:00:00" description="Example scenario for follow trajectory with route points defined in LanePosition and without time reference set on a crossing" author="ANSYS" />
   <ParameterDeclarations>
      <!--The ParameterDeclarations section is needed for easy variation.-->
      <ParameterDeclaration name="scenario_duration_s" parameterType="double" value="30" />
      <ParameterDeclaration name="pedestrian_initial_velocity_mps" parameterType="double" value="1.0" />
      <ParameterDeclaration name="ego_waypoint_soffset_m" parameterType="double" value="3.0" />
      <ParameterDeclaration name="ego_waypoint_toffset_m" parameterType="double" value="0.0" />
      <ParameterDeclaration name="pedestrian_waypoint_soffset_m" parameterType="double" value="0.0" />
      <ParameterDeclaration name="pedestrian_waypoint_toffset_m" parameterType="double" value="0.0" />
   </ParameterDeclarations>
   <CatalogLocations>
      <VehicleCatalog>
         <Directory path="./Catalogs/Vehicles" />
      </VehicleCatalog>
      <PedestrianCatalog>
         <Directory path="./Catalogs/Pedestrians" />
      </PedestrianCatalog>
   </CatalogLocations>
   <RoadNetwork>
      <LogicFile filepath="../../Maps/crossing_example.xodr" />
   </RoadNetwork>
   <Entities>
      <ScenarioObject name="Ego">
         <CatalogReference catalogName="VehicleCatalog" entryName="Car" />
      </ScenarioObject>
      <ScenarioObject name="Pedestrian">
         <CatalogReference catalogName="PedestrianCatalog" entryName="Male" />
      </ScenarioObject>
   </Entities>
   <Storyboard>
      <Init>
         <Actions>
            <GlobalAction>
               <EnvironmentAction>
                  <Environment name="Environment">
                     <TimeOfDay animation="true" dateTime="2021-03-18T12:00:00Z" />
                     <Weather temperature="15">
                        <Fog visualRange="70000" />
                        <Precipitation precipitationType="rain" precipitationIntensity="0" />
                     </Weather>
                  </Environment>
               </EnvironmentAction>
            </GlobalAction>
            <Private entityRef="Ego">
               <PrivateAction>
                  <TeleportAction>
                     <Position>
                        <LanePosition roadId="1" laneId="-1" offset="$ego_waypoint_toffset_m" s="$ego_waypoint_soffset_m">
                           <Orientation type="relative" />
                        </LanePosition>
                     </Position>
                  </TeleportAction>
               </PrivateAction>
            </Private>
            <Private entityRef="Pedestrian">
               <PrivateAction>
                  <TeleportAction>
                     <Position>
                        <LanePosition roadId="2" laneId="-1" offset="$pedestrian_waypoint_toffset_m" s="$pedestrian_waypoint_soffset_m">
                           <Orientation type="relative" />
                        </LanePosition>
                     </Position>
                  </TeleportAction>
               </PrivateAction>
               <PrivateAction>
                  <LongitudinalAction>
                     <SpeedAction>
                        <SpeedActionDynamics dynamicsShape="step" dynamicsDimension="time" value="0" />
                        <SpeedActionTarget>
                           <AbsoluteTargetSpeed value="$pedestrian_initial_velocity_mps" />
                        </SpeedActionTarget>
                     </SpeedAction>
                  </LongitudinalAction>
               </PrivateAction>
               <PrivateAction>
                  <RoutingAction>
                     <FollowTrajectoryAction>
                        <TrajectoryRef>
                           <Trajectory name="CrossTrajectory" closed="false">
                              <Shape>
                                 <Polyline>
                                    <Vertex>
                                       <Position>
                                          <LanePosition roadId="2" laneId="-1" offset="0.0" s="0.0">
                                             <Orientation h="1.57" type="relative"/>
                                          </LanePosition>
                                       </Position>
                                    </Vertex>
                                    <Vertex>
                                       <Position>
                                          <LanePosition roadId="2" laneId="-1" offset="0.0" s="5.0">
                                             <Orientation h="1.57" type="relative"/>
                                          </LanePosition>
                                       </Position>
                                    </Vertex>
                                    <Vertex>
                                       <Position>
                                          <LanePosition roadId="2" laneId="-1" offset="0.0" s="10.0">
                                             <Orientation h="1.57" type="relative"/>
                                          </LanePosition>
                                       </Position>
                                    </Vertex>
                                 </Polyline>
                              </Shape>
                           </Trajectory>
                        </TrajectoryRef>
                        <TimeReference />
                        <TrajectoryFollowingMode followingMode="position" />
                     </FollowTrajectoryAction>
                  </RoutingAction>
               </PrivateAction>
            </Private>
         </Actions>
      </Init>
      <Story name="DummyStory">
         <Act name="DummyAct">
            <ManeuverGroup maximumExecutionCount="1" name="DummyManeuverGroup">
               <Actors selectTriggeringEntities="false" />
            </ManeuverGroup>
            <StartTrigger />
         </Act>
       </Story>
      <StopTrigger>
         <ConditionGroup>
            <Condition name="Scenario_Stop_Time_Reached" delay="0" conditionEdge="rising">
               <ByValueCondition>
                  <SimulationTimeCondition rule="greaterOrEqual" value="$scenario_duration_s" />
               </ByValueCondition>
            </Condition>
         </ConditionGroup>
      </StopTrigger>
   </Storyboard>
</OpenSCENARIO>