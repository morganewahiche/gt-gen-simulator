/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_VERSION_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_VERSION_H

#include <string>

namespace gtgen::simulator
{

///@brief Returns the current gtgen core lib version
std::string GetSimulatorVersion();

}  // namespace gtgen::simulator

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_VERSION_H
