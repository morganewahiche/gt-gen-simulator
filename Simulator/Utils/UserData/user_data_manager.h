/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_USERDATAMANAGER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_USERDATAMANAGER_H

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/UserSettings/user_settings_handler.h"
#include "Core/Service/Utility/version.h"
#include "Simulator/Utils/UserData/Internal/path_helper.h"
#include "Simulator/Utils/UserData/data_path_handler.h"

#include <string>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

/// @brief Triggers installation or upgrade of User Data
///
/// The manager behaves as follows:
///
/// - no custom path has been provided:
///     - default GTGEN_DATA path doesn't exist: user data will be installed
///     - default GTGEN_DATA path exists: user data will be checked and repaired or upgraded if possible
/// - a custom path has been provided:
///     - custom path doesn't exist: exception is thrown
///     - path exists: user data will be checked
///
/// Install/repair/upgrade of a custom path can be done with gtgen_cli -i -d /path/to/data
///
class UserDataManager
{
  public:
    /// @brief Creates the UserDataManager.
    /// @param custom_gtgen_data_directory  Custom gtgen data path to use. If empty then the default GTGEN_DATA (in home
    /// directory) will be used.
    /// @param custom_user_settings Custom user settings file. If empty then default to GTGEN_DATA UserSettings.ini
    /// @param get_gtgen_data_directory Function pointer to get gtgen data directory.
    explicit UserDataManager(
        const fs::path& custom_gtgen_data,
        const fs::path& custom_user_settings,
        std::function<fs::path()> get_gtgen_data_directory = DataPathHandler::GetDefaultGtGenDataDirectory,
        std::function<fs::path()> install_source_directory = ResolveInstallationSourcePath);

    /// @brief Determines if the GTGEN_DATA needs to be installed.
    /// @return true if GTGEN_DATA in the home directory is used *and* it does not exist, false otherwise
    bool IsGtGenDataInstallationNeeded() const;

    /// @brief Installs the GTGEN_DATA.
    void Install() const;

    /// @brief Initializes the UserData
    /// @param scenario_file  Scenario file, needed to resolve the parent path, for resolving relative map paths
    /// @throws gtgen::core::simulation::simulator::SimulatorException; if the gtgen data directory or the user settings
    /// file does not exist.
    void Initialize(const std::string& scenario_file = "");

    const DataPathHandler& GetPathHandler() const;
    const gtgen::core::service::user_settings::UserSettings& GetUserSettings() const;

  private:
    bool IsDefaultGtGenDataUsed() const;
    void SetAdditionalDirectoriesFromUserSettings();
    void InitializeUserSettings();

    DataPathHandler data_path_handler_;
    gtgen::core::service::user_settings::UserSettingsHandler user_settings_handler_;

    fs::path gtgen_data_directory_;
    fs::path user_settings_;

    std::function<fs::path()> get_gtgen_data_directory_{nullptr};
    std::function<fs::path()> install_source_directory_{nullptr};
};

}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_USERDATAMANAGER_H
