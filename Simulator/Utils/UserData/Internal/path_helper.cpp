/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/path_helper.h"

#include "Core/Service/Utility/string_utils.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

fs::path ResolveInstallationSourcePath()
{
    fs::path installation_source_path{fs::current_path() / "Core/Simulation/UserData/GTGEN_DATA/"};
    if (!fs::exists(installation_source_path))
    {
        installation_source_path = "/opt/gtgen_core/GTGEN_DATA";
    }
    return installation_source_path;
}

bool IsTestDirectory(const fs::path& path)
{
    if (!fs::is_directory(path))
    {
        return false;
    }

    auto lower_path = gtgen::core::service::utility::ToLower(path.filename().string());
    return lower_path.find("test") != std::string::npos;
}

bool IsMetaFile(const fs::path& path)
{
    return path.extension() == ".meta";
}

}  // namespace gtgen::simulator::simulation::user_data
