/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/path_helper.h"

#include "Core/Service/FileSystem/file_system_utils.h"

#include <gtest/gtest.h>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

TEST(ResolveInstallationSourcePathTest,
     GivenGtGenDataInUserDataExist_WhenResolveInstallationSourcePath_ThenReturnThisPath)
{
    auto existing_gtgen_data_in_user_data = fs::current_path() / "Core/Simulation/UserData/GTGEN_DATA/";
    gtgen::core::service::file_system::CreateDirectoryIfNotExisting(existing_gtgen_data_in_user_data);
    auto path = ResolveInstallationSourcePath();

    EXPECT_EQ(path.string(), existing_gtgen_data_in_user_data.string());

    fs::remove_all(existing_gtgen_data_in_user_data);
}

TEST(ResolveInstallationSourcePathTest,
     GivenGtGenDataInUserDataDoesNotExist_WhenResolveInstallationSourcePath_ThenReturnPathToGtGenCoreInOpt)
{
    auto path = ResolveInstallationSourcePath();

    EXPECT_EQ(path.string(), "/opt/gtgen_core/GTGEN_DATA");
}

TEST(IsMetaFileTest, GivenDifferentPaths_WhenIsMetaFile_ThenMatchingMetaFilePathDetected)
{
    auto path = fs::current_path();

    // just use a realistically looking path, the files do not need to exist
    EXPECT_TRUE(IsMetaFile(path / "afile.meta"));
    EXPECT_FALSE(IsMetaFile(path / "afile.Meta"));
    EXPECT_FALSE(IsMetaFile(path / "afile.META"));
    EXPECT_FALSE(IsMetaFile(path / "meta.file"));
}

TEST(IsTestDirectoryTest, GivenTestsFolder_WhenIsTestDirectory_ThenReturnTrue)
{
    auto test_folder =
        gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "Tests");

    EXPECT_TRUE(IsTestDirectory(test_folder));
}

TEST(IsTestDirectoryTest, GivenTestsFolderAsParent_WhenIsTestDirectory_ThenReturnFalse)
{
    auto test_folder =
        gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "Tests");
    auto parent_folder_contains_test =
        gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(test_folder, "blub");

    EXPECT_FALSE(IsTestDirectory(parent_folder_contains_test));
}

}  // namespace gtgen::simulator::simulation::user_data
