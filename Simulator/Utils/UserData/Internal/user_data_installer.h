/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAINSTALLER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAINSTALLER_H

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/version.h"
#include "Simulator/Utils/UserData/Internal/user_data_upgrader.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

class UserDataInstaller
{
  public:
    /// @brief Handles the installation or upgrade of the GTGEN_DATA
    /// @param version_to_be_installed  Current GTGen core version
    explicit UserDataInstaller(const gtgen::core::Version& version_to_be_installed);

    /// @brief Installs the GTGEN_DATA or upgrades it when an older version is already existing
    /// @param installation_path  Path to where GTGEN_DATA shall be installed
    /// @param installation_data_source_path  Path where to original GTGEN_DATA is located
    /// @param user_settings   Path to where the user settings ini is located. Can be either a custom path or within
    ///                        GTGEN_DATA, but should be valid.
    void InstallOrUpgrade(const fs::path& installation_path,
                          const fs::path& installation_data_source_path,
                          const fs::path& user_settings);

  private:
    void BackupLegacyGtGenData(const fs::path& installation_path) const;
    void InstallDirectoryRecursive(const fs::path& from_directory, const fs::path& to_directory);
    void InstallFile(const fs::path& source_file_path, const fs::path& target_file_path);

    void InstallUserSettings(const fs::path& user_settings) const;

    UserDataUpgrader user_data_upgrader_;
    gtgen::core::Version version_to_install_;

    std::uint32_t installed_file_count_{0};
};

}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAINSTALLER_H
