/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_DATAPATHHANDLER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_DATAPATHHANDLER_H

#include "Core/Service/FileSystem/filesystem.h"

#include <string>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

class DataPathHandler
{
  public:
    void SetGtGenDataDirectory(std::string gtgen_data_directory);
    void SetBasePath(std::string base_path);
    void SetAdditionalScenarioDirectories(const std::vector<std::string>& additional_directories);
    void SetAdditionalMapDirectories(const std::vector<std::string>& additional_directories);

    /// @brief Returns the path to the GTGEN_DATA directory
    fs::path GetGtGenDataDirectory() const;

    /// @brief Returns the path to the default GTGEN_DATA directory
    static fs::path GetDefaultGtGenDataDirectory();

    /// @brief Returns the path to the user settings directory
    fs::path GetGtGenDataConfigDirectory() const;

    /// @brief Returns the file path of the user settings file
    fs::path GetGtGenDataUserSettingsFile() const;

    /// @brief Returns the path to the default log directory
    fs::path GetGtGenDataLogDirectory() const;

    /// @brief Returns the path that contains the packaged open scenario files for a default GTGen installation
    fs::path GetGtGenDataOpenScenarioDirectory() const;

    /// @brief Returns the path that contains the packaged map files for a default GTGen installation
    fs::path GetGtGenDataMapsDirectory() const;

    /// @brief Determines the absolute path for a given input path and a base_path
    ///
    /// Depending on how input_path is given, the path resolution takes the following steps:
    /// - If the given path is absolute, no further action is taken
    /// - If the given path is relative the following steps are taken in the stated order:
    ///   1. If the file is found in the working directory, this path is returned.
    ///   2. If the file is found relative to the base_path, this path is returned.
    ///   3. If additional search directories for either a scenario- or map-files are defined, these folders are
    ///      searched recursively.
    ///   5. Finally the GTGEN_DATA folder is recursively searched, if the input-path represents a scenario or a map.
    ///
    /// If none of the above checks find the desired input_path, the input_path is returned 'as is' for error reporting.
    ///
    /// @param input_path File to search for, either only filename or with a path.
    /// @param base_path Directory for relative lookup.
    /// @return Absolute path to the input file, or 'input_path' if the file was nowhere found.
    fs::path ResolvePathOrThrow(const fs::path& input_path, const fs::path& base_path = "") const;

  private:
    fs::path gtgen_data_directory_;
    fs::path base_path_;

    std::vector<fs::path> custom_scenario_directories_{};
    std::vector<fs::path> custom_map_directories_{};

    bool data_directory_set_{false};
};
}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_DATAPATHHANDLER_H
