/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_VERSION_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_VERSION_H

#include "Core/Service/Utility/version.h"

namespace gtgen::simulator
{

/// @brief This is the current GTGEN Simulator version. Changes must be applied in the packaging BUILD file accordingly.
constexpr gtgen::core::Version gtgen_simulator_version{GTGEN_SIMULATOR_VERSION_MAJOR,
                                                       GTGEN_SIMULATOR_VERSION_MINOR,
                                                       GTGEN_SIMULATOR_VERSION_PATCH};

}  // namespace gtgen::simulator

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_VERSION_H
