/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Export/simulator.h"

#include "Simulator/LibSimCore/simulator_impl.h"

#include <memory>

namespace gtgen::simulator
{
Simulator::Simulator(gtgen::core::SimulationParameters params)  // NOLINT(performance-unnecessary-value-param)
    : impl_{std::make_unique<SimulatorImpl>(params)}
{
}

Simulator::~Simulator() = default;

void Simulator::Init()
{
    impl_->Init();
}

void Simulator::Step()
{
    impl_->Step();
}

bool Simulator::IsFinished() const
{
    return impl_->IsFinished();
}

void Simulator::Shutdown()
{
    impl_->Shutdown();
}

std::chrono::milliseconds Simulator::GetStepSize() const
{
    return impl_->GetStepSize();
}

double Simulator::GetTimeScale() const
{
    return impl_->GetTimeScale();
}

std::chrono::milliseconds Simulator::GetScenarioDuration() const
{
    return impl_->GetScenarioDuration();
}

const osi3::SensorView& Simulator::GetSensorView() const
{
    return impl_->GetSensorView();
}

const std::vector<osi3::TrafficCommand>& Simulator::GetTrafficCommands() const
{
    return impl_->GetTrafficCommands();
}

void Simulator::SetTrafficUpdate(osi3::TrafficUpdate traffic_update)  // NOLINT(performance-unnecessary-value-param)
{
    impl_->SetTrafficUpdate(traffic_update);  // NOLINT(performance-unnecessary-value-param)
}

void Simulator::SetDriverRelatedData(gtgen::core::messages::ui::DriverRelatedData driver_related_data)
{
    impl_->SetDriverRelatedData(std::move(driver_related_data));
}

const gtgen::core::IHostVehicleInterface& Simulator::GetHostVehicleInterface() const
{
    return impl_->GetHostVehicleInterface();
}

int Simulator::ValidateMap() const
{
    throw std::runtime_error("ValidateMap() function has been deprecated and will be removed soon.");
}

int Simulator::ValidateScenario(const gtgen::core::SimulationParameters& simulation_parameters)
{
    return SimulatorImpl::ValidateScenario(simulation_parameters);
}

void Simulator::InstallUserData() const
{
    throw std::runtime_error("InstallUserData() function has been deprecated and will be removed soon.");
}

}  // namespace gtgen::simulator
