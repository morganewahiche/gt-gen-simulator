/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Export/simulation_controller.h"

#include <gtest/gtest.h>

namespace gtgen::simulator
{

// Note: This test is mainly for compiling and linking against the exported lib sim core symbols
TEST(LibSimCoreSymbolsTest, GivenSimulationController_WhenAccessAPI_ThenNoExceptionThrown)
{
    SimulationController simulation_controller("Info");

    EXPECT_NO_THROW(simulation_controller.ExecuteStep());
    EXPECT_NO_THROW(simulation_controller.SetSimulationStepCallback({}));
    EXPECT_NO_THROW(simulation_controller.SetSimulationAbortCallback({}));
    EXPECT_NO_THROW(simulation_controller.GetServerPort());
}

}  // namespace gtgen::simulator
