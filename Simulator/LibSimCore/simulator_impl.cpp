/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/LibSimCore/simulator_impl.h"

#include "Core/Environment/GtGenEnvironment/environment_factory.h"
#include "Core/Service/Logging/log_setup.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Core/Simulation/Simulator/simulator_core.h"
#include "Simulator/MapEngines/MapEngineFactory/map_engine_factory.h"
#include "Simulator/ScenarioEngines/ScenarioEngineFactory/scenario_engine_factory.h"
#include "Simulator/Utils/Logging/logging.h"

#include <memory>

namespace gtgen::simulator
{

using gtgen::core::simulation::simulator::SimulatorException;

namespace detail
{
std::shared_ptr<simulation::user_data::UserDataManager> InitializeUserDataManager(const SimulationParameters& params)
{
    auto user_data_manager = std::make_shared<simulation::user_data::UserDataManager>(
        params.custom_gtgen_data_directory, params.custom_user_settings_path);

    if (user_data_manager->IsGtGenDataInstallationNeeded())
    {
        user_data_manager->Install();
    }
    user_data_manager->Initialize(params.scenario);

    return user_data_manager;
}
}  // namespace detail

SimulatorImpl::SimulatorImpl(SimulationParameters params)  // NOLINT(performance-unnecessary-value-param)
    : params_{std::move(params)}
{
    gtgen::core::service::logging::LogSetup::Instance().AddConsoleLogger(params_.console_log_level);

    user_data_manager_ = detail::InitializeUserDataManager(params_);

    environment_ = gtgen::core::environment::api::EnvironmentFactory::Create(
        user_data_manager_->GetUserSettings(), mantle_api::Time{static_cast<double>(params_.step_size_ms)});

    auto scenario_engine = CreateScenarioEngine();

    // TODO: Remove additional scenario parsing to get map path. Instantiate map engine in Init after scenario engine
    // was initialized and scenario is parsed anyway (before environment is initialized!)
    auto num_errors = scenario_engine->ValidateScenario();
    if (num_errors != 0)
    {
        throw SimulatorException("{} errors when validating scenario.", num_errors);
    }

    auto scenario_info = scenario_engine->GetScenarioInfo();
    if (scenario_info.full_map_path == "")
    {
        throw SimulatorException(
            "Map path could not be resolved from scenario. Cannot proceed with creation of map engine.");
    }
    environment_->SetMapEngine(MapEngineFactory::Create(scenario_info.full_map_path));

    // @todo temporary solution to pass log directory over to Core
    auto user_settings = user_data_manager_->GetUserSettings();
    user_settings.gtgen_log_directory = user_data_manager_->GetPathHandler().GetGtGenDataLogDirectory();

    core_ = std::make_unique<gtgen::core::simulation::simulator::SimulatorCore>(
        std::move(scenario_engine), environment_, params_, user_settings);
}

SimulatorImpl::~SimulatorImpl() = default;

void SimulatorImpl::Init()
{
    core_->Init();
}

std::unique_ptr<mantle_api::IScenarioEngine> SimulatorImpl::CreateScenarioEngine() const
{
    auto scenario_engine = ScenarioEngineFactory::Create(params_.scenario, environment_, user_data_manager_.get());
    if (user_data_manager_->GetUserSettings().host_vehicle.host_control_mode ==
        gtgen::core::service::user_settings::HostControlMode::kExternal)
    {
        scenario_engine->ActivateExternalHostControl();
    }

    return scenario_engine;
}

void SimulatorImpl::Step()
{
    core_->Step();
}

bool SimulatorImpl::IsFinished() const
{
    return core_->IsFinished();
}

void SimulatorImpl::Shutdown()
{
    core_->Shutdown();
}

std::chrono::milliseconds SimulatorImpl::GetStepSize() const
{
    return core_->GetStepSize();
}

double SimulatorImpl::GetTimeScale() const
{
    return core_->GetTimeScale();
}

std::chrono::milliseconds SimulatorImpl::GetScenarioDuration() const
{
    return core_->GetScenarioDuration();
}

const osi3::SensorView& SimulatorImpl::GetSensorView() const
{
    return core_->GetSensorView();
}

const std::vector<osi3::TrafficCommand>& SimulatorImpl::GetTrafficCommands() const
{
    return core_->GetTrafficCommands();
}

void SimulatorImpl::SetTrafficUpdate(const osi3::TrafficUpdate& traffic_update)
{
    core_->SetTrafficUpdate(traffic_update);
}

void SimulatorImpl::SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data)
{
    core_->SetDriverRelatedData(std::move(driver_related_data));
}

const gtgen::core::IHostVehicleInterface& SimulatorImpl::GetHostVehicleInterface() const
{
    return core_->GetHostVehicleInterface();
}

int SimulatorImpl::ValidateScenario(const SimulationParameters& simulation_parameters)
{
    gtgen::core::service::logging::LogSetup::Instance().AddConsoleLogger(simulation_parameters.console_log_level);

    auto user_data_manager = std::make_unique<simulation::user_data::UserDataManager>(
        simulation_parameters.custom_gtgen_data_directory, simulation_parameters.custom_user_settings_path);

    std::shared_ptr<mantle_api::IEnvironment> environment = gtgen::core::environment::api::EnvironmentFactory::Create(
        user_data_manager->GetUserSettings(),
        mantle_api::Time{static_cast<double>(simulation_parameters.step_size_ms)});

    auto scenario_engine =
        ScenarioEngineFactory::Create(simulation_parameters.scenario, environment, user_data_manager.get());

    return scenario_engine->ValidateScenario();
}

std::uint16_t SimulatorImpl::GetServerPort() const
{
    return core_->GetServerPort();
}

}  // namespace gtgen::simulator
