/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ODRTOGTGENMAPCONVERTERCREATOR_H
#define GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ODRTOGTGENMAPCONVERTERCREATOR_H

#include "Core/Environment/Map/Common/i_any_to_gtgenmap_converter.h"
#include "Core/Environment/Map/Common/map_converter_data.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Service/Utility/unique_id_provider.h"

namespace gtgen::simulator::environment::map::open_drive
{
using gtgen::core::environment::map::GtGenMap;
using gtgen::core::environment::map::IAnyToGtGenMapConverter;
using gtgen::core::environment::map::MapConverterData;
namespace service = gtgen::core::service;

class OdrToGtGenMapConverterCreator
{
  public:
    static std::unique_ptr<IAnyToGtGenMapConverter> Create(
        gtgen::core::service::utility::UniqueIdProvider& unique_id_provider,
        const MapConverterData& data,
        GtGenMap& gtgen_map);
};

}  // namespace gtgen::simulator::environment::map::open_drive

#endif  // GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ODRTOGTGENMAPCONVERTERCREATOR_H
