/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/Internal/opendrive_to_gtgenmap_coord_converter.h"

#include "Core/Environment/Exception/exception.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::environment::map::open_drive
{
using gtgen::core::environment::EnvironmentException;

OpenDriveToGtGenMapCoordConverter::OpenDriveToGtGenMapCoordConverter(
    const std::shared_ptr<road_logic_suite::RoadLogicSuite>& road_logic_suite)
    : road_logic_suite_(road_logic_suite)
{
}

std::optional<mantle_api::Vec3<units::length::meter_t>> OpenDriveToGtGenMapCoordConverter::Convert(
    const UnderlyingMapCoordinate& coordinate) const
{
    if (!std::holds_alternative<mantle_api::OpenDriveLanePosition>(coordinate))
    {
        LogAndThrow(EnvironmentException("Tried to convert LatLong position with OpenDrive converter."));
    }
    const auto odr_position = std::get<mantle_api::OpenDriveLanePosition>(coordinate);
    return road_logic_suite_->ConvertLaneToInertialCoordinates(odr_position);
}

std::optional<UnderlyingMapCoordinate> OpenDriveToGtGenMapCoordConverter::Convert(
    const mantle_api::Vec3<units::length::meter_t>& coordinate) const
{
    return road_logic_suite_->ConvertInertialToLaneCoordinates(coordinate);
}

}  // namespace gtgen::simulator::environment::map::open_drive
