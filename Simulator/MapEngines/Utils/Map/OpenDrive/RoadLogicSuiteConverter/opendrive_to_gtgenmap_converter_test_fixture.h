/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTERTESTFIXTURE_H
#define GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTERTESTFIXTURE_H

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/unique_id_provider.h"
#include "Core/Tests/TestUtils/expect_extensions.h"
#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/opendrive_to_gtgenmap_converter.h"
#include "Simulator/Utils/Logging/logging.h"

#include <gtest/gtest.h>

#include <memory>

namespace gtgen::simulator::environment::map::open_drive
{
namespace fs = gtgen::core::fs;
namespace map = gtgen::core::environment::map;
using gtgen::core::environment::map::GtGenMap;
using gtgen::core::environment::map::Lane;
using gtgen::core::environment::map::LaneBoundary;

class OpendriveToGtGenMapConverterTest : public testing::Test
{
  public:
    GtGenMap& GetGtGenMap(const std::string& map_name,
                          double distance_in_m = 0.4,
                          double epsilon = 0.01,
                          bool downsampling = true)
    {
        unique_id_provider_.Reset();

        spdlog::set_level(spdlog::level::info);
        Info("Testing map {}", map_name);
        spdlog::set_level(spdlog::level::err);

        map::MapConverterData data;
        data.open_drive_properties.lane_marking_distance_in_m = distance_in_m;
        data.open_drive_properties.lane_marking_downsampling_epsilon = epsilon;
        data.open_drive_properties.lane_marking_downsampling = downsampling;

        data.absolute_map_path = fs::path("Simulator/Tests/Data/Maps") / map_name;

        gtgen_map = std::make_unique<GtGenMap>();

        converter = std::make_unique<OpendriveToGtGenMapConverter>(unique_id_provider_, data, *gtgen_map);
        converter->Convert();

        return *gtgen_map;
    }

    std::unique_ptr<OpendriveToGtGenMapConverter> converter;
    std::unique_ptr<GtGenMap> gtgen_map{nullptr};

  private:
    gtgen::core::service::utility::UniqueIdProvider unique_id_provider_{};
};

}  // namespace gtgen::simulator::environment::map::open_drive
#endif  // GTGEN_SIMULATOR_MAPENGINES_UTILS_MAP_OPENDRIVE_ROADLOGICSUITECONVERTER_OPENDRIVETOGTGENMAPCONVERTERTESTFIXTURE_H
