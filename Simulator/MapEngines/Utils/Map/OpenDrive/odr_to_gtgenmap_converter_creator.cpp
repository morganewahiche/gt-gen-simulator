/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/Utils/Map/OpenDrive/odr_to_gtgenmap_converter_creator.h"

#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/opendrive_to_gtgenmap_converter.h"

namespace gtgen::simulator::environment::map::open_drive
{

std::unique_ptr<IAnyToGtGenMapConverter> OdrToGtGenMapConverterCreator::Create(
    gtgen::core::service::utility::UniqueIdProvider& unique_id_provider,
    const MapConverterData& data,
    GtGenMap& gtgen_map)
{
    return std::make_unique<environment::map::open_drive::OpendriveToGtGenMapConverter>(
        unique_id_provider, data, gtgen_map);
}

}  // namespace gtgen::simulator::environment::map::open_drive
