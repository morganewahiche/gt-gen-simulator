/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/MapEngineFactory/map_engine_factory.h"

#include "Core/Environment/Exception/exception.h"
#include "Simulator/MapEngines/MapEngineOdr/map_engine_odr.h"

#include <gtest/gtest.h>
namespace gtgen::simulator
{

TEST(MapEngineFactoryTest, GivenXodrMap_WhenCreatingEngine_ThenXodrMapEngineCreated)
{

    std::string map_path{"my/fantasy/path/map.xodr"};
    auto concrete_engine = MapEngineFactory::Create(map_path);

    environment::api::MapEngineOdr* odr_engine = dynamic_cast<environment::api::MapEngineOdr*>(concrete_engine.get());
    EXPECT_TRUE(odr_engine != nullptr);
}

TEST(MapEngineFactoryTest, GivenUnkownMap_WhenCreatingEngine_ThenExceptionIsThrown)
{
    std::string map_path{"my/fantasy/path/map.unknown"};
    EXPECT_THROW(MapEngineFactory::Create("map_path"), gtgen::core::environment::EnvironmentException);
}

}  // namespace gtgen::simulator
