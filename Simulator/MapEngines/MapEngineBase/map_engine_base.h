/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_MAPENGINES_MAPENGINEBASE_MAPENGINEBASE_H
#define GTGEN_SIMULATOR_MAPENGINES_MAPENGINEBASE_MAPENGINEBASE_H

#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"
#include "Core/Environment/Map/Common/map_converter_data.h"

namespace gtgen::simulator::environment::api
{
namespace map = gtgen::core::environment::map;

class MapEngineBase : public gtgen::core::environment::api::IMapEngine
{
  protected:
    map::MapConverterData ConvertData(const std::string& absolute_map_file_path,
                                      const gtgen::core::service::user_settings::UserSettings& settings,
                                      const mantle_api::MapDetails& map_details);

    virtual map::MapConverterData FillConverterData(const gtgen::core::service::user_settings::UserSettings& settings,
                                                    const std::string& absolute_map_file_path,
                                                    const mantle_api::MapDetails& map_details) = 0;
};

}  // namespace gtgen::simulator::environment::api
#endif  // GTGEN_SIMULATOR_MAPENGINES_MAPENGINEBASE_MAPENGINEBASE_H
