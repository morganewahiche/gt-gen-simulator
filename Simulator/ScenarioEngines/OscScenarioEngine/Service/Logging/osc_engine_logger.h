/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_SERVICE_LOGGING_OSCENGINELOGGER_H
#define GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_SERVICE_LOGGING_OSCENGINELOGGER_H

#include <MantleAPI/Common/i_logger.h>

namespace gtgen::simulator::osc_scenario_engine
{

class OscEngineLogger : public mantle_api::ILogger
{
  public:
    OscEngineLogger() = default;

    /// Get the current log level from the logging interface
    /// \return The current log level
    [[nodiscard]] mantle_api::LogLevel GetCurrentLogLevel() const noexcept override;

    /// Log a message on the logging interface
    /// \param[in] level The log level of the message
    /// \param[in] message The log message
    void Log(mantle_api::LogLevel level, std::string_view message) noexcept override;
};

}  // namespace gtgen::simulator::osc_scenario_engine

#endif  // GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_SERVICE_LOGGING_OSCENGINELOGGER_H
