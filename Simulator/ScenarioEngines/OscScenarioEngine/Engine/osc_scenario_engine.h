/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_ENGINE_OSCSCENARIOENGINE_H
#define GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_ENGINE_OSCSCENARIOENGINE_H

#include "Simulator/Utils/UserData/data_path_handler.h"

#include <OpenScenarioEngine/OpenScenarioEngine.h>

#include <memory>
#include <string>

namespace gtgen::simulator::osc_scenario_engine
{
class OSCScenarioEngine : public OpenScenarioEngine::v1_2::OpenScenarioEngine
{
  public:
    OSCScenarioEngine(const std::string& scenario_file_path,
                      const simulation::user_data::DataPathHandler& data_path_handler,
                      std::shared_ptr<mantle_api::IEnvironment> environment);

  protected:
    /// @brief Overridden for custom scenario path resolving
    std::string ResolveScenarioPath(const std::string& scenario_path) const override;
    /// @brief Overridden for custom map path resolving
    std::string ResolveMapPath(const std::string& map_path) const override;

  private:
    const simulation::user_data::DataPathHandler& data_path_handler_;
};

}  // namespace gtgen::simulator::osc_scenario_engine

#endif  // GTGEN_SIMULATOR_SCENARIOENGINES_OSCSCENARIOENGINE_ENGINE_OSCSCENARIOENGINE_H
