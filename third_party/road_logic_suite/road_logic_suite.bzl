load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "v2.5.0"

def road_logic_suite():
    maybe(
        http_archive,
        name = "road_logic_suite",
        build_file = "//third_party/road_logic_suite:road_logic_suite.BUILD",
        sha256 = "c29f503fd0a0f5ff01a9a4826cc27aea7717d8d496c4b8295b8819a1a9d51f66",
        urls = [
            "https://ci.eclipse.org/openpass/job/RoadLogicSuite/view/tags/job/{version}/lastSuccessfulBuild/artifact/artifacts/road-logic-suite-{version}-linux-x86_64.tar.gz".format(version = _VERSION),
        ],
    )
