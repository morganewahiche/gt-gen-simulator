load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v3.0.0"

def open_scenario_engine():
    maybe(
        http_archive,
        name = "open_scenario_engine",
        url = "https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/archive/{tag}/openscenario1_engine-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "a3468aa1dfdfd92b453da65000d841f0e21a17e3f1a9027c46fd3de6fd773450",
        strip_prefix = "openscenario1_engine-{tag}/engine".format(tag = _TAG),
        type = "tar.gz",
    )
