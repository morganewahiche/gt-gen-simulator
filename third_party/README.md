# Third Party Libraries in use

## Directly included third-party Libraries

### bazel
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/bazel
- Version: 6.2.1

### bazel_rules
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/rules_pkg
- Version: patch version #TODO check version

### clara (Clara Command line parser)
- License: Boost Software License 1.0
- Repository: https://github.com/catchorg/Clara
- Version: 1.1.5

### googletest (Google Test)
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/google/googletest
- Version: 1.10.0

### GTGenCore
- License: Eclipse Public License 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/tags/v1.0.0

### OpenSCENARIO API (Parser)
- License: Apache-2.0
- Repository: https://github.com/RA-Consulting-GmbH/openscenario.api.test
- Commit: 94cc39a6caad0418c71aadc6567fa200ded0ac65
- Version: 1.2.1
- Info: Update and deploy new version to ddad see https://cc-github.bmwgroup.net/swh/astas/wiki/OpenScenarioParser
-
### OpenScenarioEngine
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/tags/v3.0.0


### RoadLogicSuite
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/RoadLogicSuite
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite/-/releases/v2.3.0

### Yase
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/yase
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/yase/-/tags/v0.0.1


## Indirect included third-party Libraries

### Third-party from GT-Gen-Core :
https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/blob/main/third_party/README.md




# Licenses in use

| License                                 |    Type     |                           Additional information/comment                           |
| --------------------------------------- | :---------: | :--------------------------------------------------------------------------------: |
| Apache-2.0                              | Open Source |                        free license, compatible with GPLv3                         |
| MIT License                             | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| Boost License 1.0                       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 2-Clause "Simplified" License       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 3-Clause "New" or "Revised" License | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| The Happy Bunny License and MIT License | Open Source |                     Happy Bunny License (modified MIT License)                     |
| Zlib License                            | Open Source |                           free license, without copyleft                           |
| Eclipse Public License 2.0              | Open Source | free license, **weak copyleft**, additions to the same module have to be published |
