load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

def open_scenario_parser():
    maybe(
        http_archive,
        name = "open_scenario_parser",
        build_file = "//third_party/open_scenario_parser:open_scenario_parser.BUILD",
        url = "https://github.com/RA-Consulting-GmbH/openscenario.api.test/releases/download/v1.3.0/OpenSCENARIO_API_LinuxSharedRelease_2022.10.19.tgz",
        sha256 = "a15ee746f75d49291bc7068e4193d03b5c3e8bb81c7676885d207428f7536aa6",
        strip_prefix = "OpenSCENARIO_API_LinuxSharedRelease",
    )
