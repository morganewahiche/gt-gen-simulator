/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/command_line_parser.h"

#include <gtest/gtest.h>

#include <array>

namespace gtgen::simulator::cli
{
/// @test Tests the command line parser does not start if no arguments are given
TEST(CommandLineParserTest, GivenNoArguments_WhenParse_ThenNoSimulationStartUp)
{
    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    {
        std::array<const char*, 1> argv = {s1.data()};
        EXPECT_FALSE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_EQ(cmd_line_parser.GetParserMessage(),
                  std::string("Missing mandatory scenario file. Please specify the:\n - scenario file with "
                              "[-s|--scenario] path/to/scenario.xosc"));
    }
}

/// @test Tests that the short command line arguments are supported (single dash + 1 character, i.e. '-s')
TEST(CommandLineParserTest, GivenValidShortArgs_WhenParse_ThenSimulationStartupAndParamsFilled)
{
    std::string scenario{"myScenario.xosc"};
    std::string log_directory{"Some-Log-Folder"};
    std::string use_user_settings{"/path/to/ini.ini"};
    std::string use_gt_gen_data{"/path/to/GTGEN_DATA"};
    std::string map_to_validate{"path/to/map.xodr"};
    std::uint16_t step_size_ms{42};
    std::string step_size_ms_str{std::to_string(step_size_ms)};
    std::string console_log_level{"Error"};
    bool install_user_data{true};
    bool validate_scenario{true};

    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    std::string s2{"-s"};
    std::string s3{"-l"};
    std::string s4{"-u"};
    std::string s5{"-d"};
    std::string s6{"-i"};
    std::string s7{"-m"};
    std::string s8{"-j"};
    std::string s10{"-t"};
    std::string s11{"-c"};
    {
        std::array<const char*, 17> argv = {s1.data(),
                                            s2.data(),
                                            scenario.data(),
                                            s3.data(),
                                            log_directory.data(),
                                            s4.data(),
                                            use_user_settings.data(),
                                            s5.data(),
                                            use_gt_gen_data.data(),
                                            s6.data(),
                                            s7.data(),
                                            map_to_validate.data(),
                                            s8.data(),
                                            s10.data(),
                                            step_size_ms_str.data(),
                                            s11.data(),
                                            console_log_level.data()};
        EXPECT_TRUE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_TRUE(cmd_line_parser.GetParserMessage().empty());
        EXPECT_EQ(scenario, cmd_line_parser.GetSimulationParameters().scenario);
        EXPECT_EQ(log_directory, cmd_line_parser.GetSimulationParameters().custom_log_directory);
        EXPECT_EQ(use_user_settings, cmd_line_parser.GetSimulationParameters().custom_user_settings_path);
        EXPECT_EQ(use_gt_gen_data, cmd_line_parser.GetSimulationParameters().custom_gtgen_data_directory);
        EXPECT_EQ(install_user_data, cmd_line_parser.ShouldInstallUserData());
        EXPECT_EQ(map_to_validate, cmd_line_parser.GetSimulationParameters().map_path);
        EXPECT_EQ(validate_scenario, cmd_line_parser.ShouldValidateScenario());
        EXPECT_EQ(step_size_ms, cmd_line_parser.GetSimulationParameters().step_size_ms);
        EXPECT_EQ(console_log_level, cmd_line_parser.GetSimulationParameters().console_log_level);
    }
}

/// @test Tests that the long command line arguments are supported (double dash + name, i.e. '--scenario')
TEST(CommandLineParserTest, GivenValidLongArgs_WhenParse_ThenSimulationStartupAndParamsFilled)
{
    std::string scenario{"myScenario.xosc"};
    std::string log_directory{"Some-Log-Folder"};
    std::string use_user_settings{"/path/to/ini.ini"};
    std::string use_gt_gen_data{"/path/to/GTGEN_DATA"};
    std::string map_to_validate{"path/to/map.xodr"};
    std::uint16_t step_size_ms{42};
    std::string step_size_ms_str{std::to_string(step_size_ms)};
    std::string console_log_level{"Off"};
    bool install_user_data{true};
    bool validate_scenario{true};

    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    std::string s2{"--scenario"};
    std::string s3{"--log-file-dir"};
    std::string s4{"--user-settings-file"};
    std::string s5{"--gtgen-data"};
    std::string s6{"--install"};
    std::string s7{"--validate-map"};
    std::string s8{"--validate-scenario"};
    std::string s10{"--step-size-ms"};
    std::string s11{"--console-log-level"};
    {
        std::array<const char*, 17> argv = {s1.data(),
                                            s2.data(),
                                            scenario.data(),
                                            s3.data(),
                                            log_directory.data(),
                                            s4.data(),
                                            use_user_settings.data(),
                                            s5.data(),
                                            use_gt_gen_data.data(),
                                            s6.data(),
                                            s7.data(),
                                            map_to_validate.data(),
                                            s8.data(),
                                            s10.data(),
                                            step_size_ms_str.data(),
                                            s11.data(),
                                            console_log_level.data()};
        EXPECT_TRUE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_TRUE(cmd_line_parser.GetParserMessage().empty());
        EXPECT_EQ(scenario, cmd_line_parser.GetSimulationParameters().scenario);
        EXPECT_EQ(log_directory, cmd_line_parser.GetSimulationParameters().custom_log_directory);
        EXPECT_EQ(use_user_settings, cmd_line_parser.GetSimulationParameters().custom_user_settings_path);
        EXPECT_EQ(use_gt_gen_data, cmd_line_parser.GetSimulationParameters().custom_gtgen_data_directory);
        EXPECT_EQ(install_user_data, cmd_line_parser.ShouldInstallUserData());
        EXPECT_EQ(map_to_validate, cmd_line_parser.GetSimulationParameters().map_path);
        EXPECT_EQ(validate_scenario, cmd_line_parser.ShouldValidateScenario());
        EXPECT_EQ(step_size_ms, cmd_line_parser.GetSimulationParameters().step_size_ms);
        EXPECT_EQ(console_log_level, cmd_line_parser.GetSimulationParameters().console_log_level);
    }
}

/// @test Tests that the help is printed when -?, -h or --help is provided
TEST(CommandLineParserTest, GivenHelpArgument_WhenParse_ThenHelpPrinted)
{
    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    std::string s2{"-h"};
    std::string s3{"-?"};
    std::string s4{"--help"};
    {
        std::array<const char*, 2> argv = {s1.data(), s2.data()};
        EXPECT_TRUE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("usage:"), std::string::npos);
    }

    {
        std::array<const char*, 2> argv = {s1.data(), s3.data()};
        cmd_line_parser.Parse(argv.size(), argv.data());
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("usage:"), std::string::npos);
    }
    {
        std::array<const char*, 2> argv = {s1.data(), s4.data()};
        cmd_line_parser.Parse(argv.size(), argv.data());
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("usage:"), std::string::npos);
    }
}

/// @test Tests that the version is printed when -v or --version is provided
TEST(CommandLineParserTest, GivenVersionArgument_WhenParse_ThenVersionPrinted)
{
    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    std::string s2{"--version"};
    std::string s3{"-v"};
    {
        std::array<const char*, 2> argv = {s1.data(), s2.data()};
        EXPECT_TRUE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("Core:"), std::string::npos);
    }

    {
        std::array<const char*, 2> argv = {s1.data(), s3.data()};
        EXPECT_TRUE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("Cli:"), std::string::npos);
    }
}

/// @test Tests that the parsing fails when an invalid argument is provided
TEST(CommandLineParserTest, GivenInvalidArguments_WhenParse_ThenParsingFails)
{
    CommandLineParser cmd_line_parser{};
    std::string s1{"Program_name"};
    std::string s2{"-bullshit"};
    {
        std::array<const char*, 2> argv = {s1.data(), s2.data()};
        EXPECT_FALSE(cmd_line_parser.Parse(argv.size(), argv.data()));
        EXPECT_NE(cmd_line_parser.GetParserMessage().find("bullshit"), std::string::npos);
    }
}

}  // namespace gtgen::simulator::cli
