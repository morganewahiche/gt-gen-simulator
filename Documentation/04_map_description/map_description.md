## Maps


GT-Gen Simulator supports the ASAM OpenDRIVE® format, `.xodr`, facilitating the representation of complex road networks through the [RoadLogicSuite](https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite), a cornerstone of the simulator's map processing capabilities.


### RoadLogicSuite: The Core of Map Engine

[RoadLogicSuite](https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite), the default map engine for GT-Gen-Simulator, is a robust C++ library designed to handle ASAM OpenDRIVE map files. It plays a crucial role in ensuring that maps are accurately validated and loaded into the simulator, providing a seamless transition from map files to virtual road networks within GT-Gen-Simulator.

One of RoadLogicSuite's key features is its ability to manage coordinate transformations, which is vital for accurately simulating vehicle movements across different coordinate systems within the simulation environment.

To explore the breadth of OpenDRIVE® features supported by GT-Gen Simulator, the RoadLogicSuite documentation offers an extensive overview.


### Map Sources and Paths

Maps can be specified and loaded into the GT-Gen Simulator using different methods, ensuring flexibility in how simulation environments are set up:

- Relative Paths: Maps located relative to the scenario file can be easily referenced, allowing for organized project structures.
- Absolute Paths: Directly specify the path to your map file within the scenario file for clarity and direct access.
- Custom Paths: Users have the option to add additional map paths, whether absolute or relative to the current working directory. In scenarios where no path is specified, GT-Gen defaults to a predefined map database path.


### Referencing Maps in Scenarios

To incorporate a map into your OpenScenario (.xosc) files, you simply reference the map's location within the <RoadNetwork> tag. This straightforward approach allows the simulator to understand which map to load for the given scenario.

```xml
<RoadNetwork>
    <LogicFile filepath="../../Maps/ALKS/ALKS_Road.xodr" />
</RoadNetwork>
```
