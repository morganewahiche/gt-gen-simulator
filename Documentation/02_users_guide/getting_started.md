## Getting Started


### Installation

For an easy setup, download the `.deb` packages from our project's [release page](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/releases). These packages are designed for quick installation on Ubuntu systems. Open a terminal and navigate to the folder where you've downloaded the packages. To install, use the dpkg command:

```bash
# Install GtGen-Simulator
dpkg -i gtgen-simulator_{version}_amd64.deb
# Install the command line tool:  GtGen-Cli
dpkg -i gtgen-cli_{version}_amd64.deb
```

After installation, the simulator library (`gtgen_core`) is located in `/opt/gtgen_core`, and the command-line tool (`gtgen_cli`) can be found at `/opt/gtgen_cli/gtgen_cli`.



### CLI Usage

With GT-Gen and CLI installed, you're ready to launch simulations directly from the command line. For instance, to run a .xosc scenario:


```bash
gtgen_cli -s scenario/ALKS_Scenario_4.4_1_CutInNoCollision.xosc
```

For a comprehensive guide on CLI options and tools, use help option `gtgen_cli -h`.
