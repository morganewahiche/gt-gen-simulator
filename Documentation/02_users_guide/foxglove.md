##  Visualization

GT-Gen enhances simulation analysis by supporting run-time visualization of simulation outputs via a WebSocket connection. This feature works seamlessly with Foxglove-Studio, offering an intuitive way to observe and analyze simulation data dynamically.


To utilize WebSocket streaming, specific settings need to be adjusted within the **UserSettings.ini** file. Detailed instructions on configuring these settings can be found in the  [Simulator Configuration](sim_config.md) section.

### Install Foxglove
Installation and configuration of Foxglove are prerequisites for visualization and are not included in the GT-Gen package.

### Open websocket connection in Foxglove
To visualize GT-Gen's simulation output in Foxglove:

1. Start Foxglove and select Open a new connection.
2. Choose Foxglove WebSocket.
3. GT-Gen will then stream simulation data to Foxglove's **default WebSocket address**, enabling real-time visualization.

![foxglove_setup](../figures/foxglove_connection.png)
