## Simulation Configuration

This section provides detailed instructions for configuring the GT-Gen Simulator using the `UserSettings.ini` configuration file. This file is pivotal for customizing the behavior of your simulator, from basic clock settings to complex ground truth generation.


### Usage

#### GT-Gen CLI
By default, the GT-Gen Simulator searches for `UserSettings.ini` in `home/GTGEN_DATA/`. For custom setups, specify the path to a different `UserSettings.ini` file via CLI options:
```
gtgen_cli -u /PATH/TO/YOUR/UserSettings.ini
```

#### Simulator Library

For projects using GT-Gen Simulator as a library, provide the path to `UserSettings.ini` through the simulator's API. Reference and guidelines are available in the exported header file, [Simulator.h](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Simulator/Export/simulator.h).


### Configuration Details
1. Clock: Toggle simulation time control.
2. HostVehicle: Configure vehicle dynamics and simulation fidelity.
3. GroundTruth: Define parameters for detailed environment simulation.
4. Map & MapChunking: Optimize map loading and detail rendering.
5. UserDirectories: Direct the simulator to specific data directories.
6. Foxglove: Set up real-time data streaming.
7. FileLogging: Manage simulation logging levels.

#### 1.Clock Settings
- **UseSystemClock**: Toggle between simulated time and real time. Default is `false`, i.e. using a `user-defined time scale`.


#### 2.Host Vehicle Configuration
- **Movement**: Choose the movement mode (`ExternalVehicle / InternalVehicle`). Default is `ExternalVehicle`.
- **BlockingCommunication**: Define if simulation steps wait for new `TrafficUpdate`. Default is `false`.
- **TimeScale**: Adjust simulation speed, where `1.0` represents real-time.
- **RecoveryMode**: Enable relocation of the host vehicle to the road after leaving it. Default is `false`.


#### 3. Ground Truth Generation
For simulations based on OpenDrive maps, configure how lane markings and ground truth are generated.

- **LaneMarkingDistance**: Defines the distance between CenterLine points and LaneBoundary, value `double`
- **SimplifyLaneMarkings**: Enables CenterLine and LaneBoundary simplification. Based on RDP algorithm, default value is `true`
- **SimplifyLaneMarkingsEpsilon**: Epsilon decides point exclusion during CenterLines and LaneBoundaries simplification, default value is `0.01`
- **AllowInvalidLaneLocations**: Determines if an error is triggered when a TrafficVehicle or Pedestrian deviates from the road without a valid LaneLocation. The HostVehicle remains unaffected. Default value is `true`

*Note: RDP is short for  Ramer-Douglas-Peucker.*


### Map
- **IncludeObstacles**: Decide whether to include obstacles in the map. Default is `false`.

### MapChunking
ChunkGridSize: The length of a side of each grid cell. Unit: `meter`, default value `50`
CellsPerDirection:  The number of cells loaded around the host vehicle's current center cell, forming concentric rings. default value `2`


### UserDirectories

Specify custom directories for scenarios, maps, and plugin files, allowing the simulator to locate user data.

- **Scenarios** : The folders in which to look for Scenarios files.
- **Maps** : The folders in which to look for map files.
- **Plugins** : The folders in which to look for Plugins files.


### Foxglove

- **WebsocketServer**: Enable streaming of ground truth data to Foxglove via a WebSocket connection. Default is `false`.



### FileLogging
- **LogLevel**: The log level used for logging to the file. Options :`Trace / Debug / Info / Warn / Error  Off`, default is `Debug`




### Example:

An example `UserSettings.ini` file is provided to kickstart your simulator setup with default values, ensuring a smooth setup process for your simulation environment.

```
[FileLogging]
LogLevel = Debug

[GroundTruth]
LaneMarkingDistance = 0.4
SimplifyLaneMarkingsEpsilon = 0.01
SimplifyLaneMarkings = true
AllowInvalidLaneLocations = true

[HostVehicle]
Movement = Internal
BlockingCommunication = false
TimeScale = 1.0
RecoveryMode = false

[Map]
IncludeObstacles = false

[MapChunking]
ChunkGridSize = 50
CellsPerDirection = 2

[UserDirectories]
Scenarios = {}
Maps = {}
Plugins = {}

[Clock]
UseSystemClock = false

[Foxglove]
WebsocketServer = false
```
